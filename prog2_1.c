#include <stdio.h>

int main() {
    printf("Assignment #2-1, Thomas Webb, tjwebb@comcast.net\n"); 
    char buffer[66];      // max length input + 1
    fgets(buffer, 66, stdin);   // take input
    int inputLen = 0;
    while (buffer[inputLen] != '\0' && inputLen < 65)
        inputLen++;
    buffer[inputLen-1] = '\0';  // terminate input and overwrite newline char
    char tokens[33][66];        // [max number of tokens + 1][max token size +1]
    char currChar;
    int currIndex = 0, tokenLen = 0, numTokens = 0;
    for (int i=0; i<33; i++) {
        currChar = buffer[currIndex];
        while (currChar != ' ' && currChar != '\0') {
            tokens[i][tokenLen++] = buffer[currIndex++];
            currChar = buffer[currIndex];
        }
        if (tokenLen != 0) {
            tokens[i][tokenLen] = '\0'; // insert terminating char at end of token
            numTokens++;
        }
        else { i--; }               // overwrite empty tokens
        tokenLen = 0;
        currIndex++;                // skip over the space
        if (currChar == '\0')
            i = 33;                 // exit loop on newline char
    }
    for (int i=0; i<numTokens; i++)
        printf("=%s=\n",tokens[i]);
    return 0;
}
