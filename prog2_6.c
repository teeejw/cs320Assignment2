#include <stdio.h>
#include <stdlib.h>

int typeTest(char array[]);

int main(int argc, char *argv[]) {
    printf("Assignment #2-6, Thomas Webb, tjwebb@comcast.net\n");
    if (argc != 2) {
        printf("ERROR! Program 6 accepts 1 command line argument.\n");
        return 0;
    }
    if (typeTest(argv[1]) != 0) {
        printf("ERROR! Expected integer argument.\n");
        return 0;
    }
    int MAX_INPUT = 65;
    int MAX_TOKEN = 33;
    int cycleCount = 0;
    int numCycles = atoi(argv[1]);

    PROMPT:
    cycleCount++;
    if (cycleCount > numCycles)
        return 0;

    printf("> ");               // prompt for user input
    char buffer[MAX_INPUT + 1];
    fgets(buffer, 66, stdin);
    int inputLen = 0;           // determine input length
    while (buffer[inputLen] != '\0' && inputLen < MAX_INPUT)
        inputLen++;
    buffer[inputLen-1] = '\0';    // add terminating character to input

    char tokens[MAX_TOKEN + 1][MAX_INPUT + 1];      
    char currChar;
    int currIndex = 0, tokenLen = 0, numTokens = 0;

    for (int i=0; i<MAX_TOKEN; i++) {   // create tokens
        currChar = buffer[currIndex];
        while (currChar != ' ' && currChar != '\0') {
            tokens[i][tokenLen++] = buffer[currIndex++];
            currChar = buffer[currIndex];
        }
        if (tokenLen != 0) {    // insert terminating char at end of token
            tokens[i][tokenLen] = '\0';
            numTokens++;
        }
        else i--;               // overwrite empty tokens
        tokenLen = 0;
        currIndex++;            // skip over the space
        if (currChar == '\0')
            i = MAX_TOKEN;      // exit loop on newline char
    }
    char quit[] = "quit";       // test if user wants to quit
    if (numTokens == 1) {
        if ((tokens[0][0] == 'q' || tokens[0][0] == 'Q') &&
            (tokens[0][1] == 'u' || tokens[0][1] == 'U') &&
            (tokens[0][2] == 'i' || tokens[0][2] == 'I') &&
            (tokens[0][3] == 't' || tokens[0][3] == 'T') &&
            (tokens[0][4] == '\0'))
            return 0;
    }
    for (int i=0; i<numTokens; i++) {   // validate token length
        int count = 0;
        while (tokens[i][count] != '\0') {
            if (count >= MAX_INPUT) {
                printf("ERROR! Input string too long.\n");
                goto PROMPT;
            }
            count++;
        }
    }
    if (numTokens == 2) {               // validate format
        if (typeTest(tokens[0]) == 1 && typeTest(tokens[1]) == 0)
            printf("STR INT\n");
        else {
            printf("ERROR! Expected STR INT.\n");
            goto PROMPT;
        }
    }
    else if (numTokens == 1) {
        if(typeTest(tokens[0]) == 1)
            printf("STR\n");
        else {
            printf("ERROR! Expected STR.\n");
            goto PROMPT;
        }
    }
    else {
        printf("ERROR! Incorrect number of tokens found.\n");
        goto PROMPT;
    }   
    goto PROMPT;
    return 0;
}

// returns 0 if INT, 1 if STR
int typeTest(char str[]) {
    int index = 0;
    int flag = 0;
    while (str[index] != '\0') {
        if (str[index] < '0' || str[index] > '9')
            flag = 1;
        index++;
    }
    return flag;
}
