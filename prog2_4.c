#include <stdio.h>

int main() {
    printf("Assignment #2-4, Thomas Webb, tjwebb@comcast.net\n");
    int MAX_INPUT = 20;
    int MAX_TOKEN = 20;

    PROMPT:    
    printf("> ");               // prompt for user input
    char buffer[MAX_INPUT + 1];
    fgets(buffer, 66, stdin);
    int inputLen = 0;
    while (buffer[inputLen] != '\0' && inputLen <= MAX_INPUT)
        inputLen++;             // get length of input
    if(buffer[inputLen] != '\0') {
        printf("ERROR! Input string too long.\n");
        goto PROMPT;
    }
    buffer[inputLen-1] = '\0';  // add terminating character to input

    char tokens[MAX_TOKEN + 1][MAX_INPUT + 1];      
    char currChar;
    int currIndex = 0, tokenLen = 0, numTokens = 0;

    for (int i=0; i<MAX_TOKEN; i++) {
        currChar = buffer[currIndex];
        while (currChar != ' ' && currChar != '\0') {
            tokens[i][tokenLen++] = buffer[currIndex++];
            currChar = buffer[currIndex];
        }
        if (tokenLen != 0) {
            tokens[i][tokenLen] = '\0'; // insert terminating char at end of token
            numTokens++;
        }
        else { i--; }           // overwrite empty tokens
        tokenLen = 0;
        currIndex++;            // skip over the space
        if (currChar == '\0')
            i = MAX_TOKEN;      // exit loop on newline char
    }
    if (numTokens == 1) {
        if ((tokens[0][0] == 'q' || tokens[0][0] == 'Q') &&
            (tokens[0][1] == 'u' || tokens[0][1] == 'U') &&
            (tokens[0][2] == 'i' || tokens[0][2] == 'I') &&
            (tokens[0][3] == 't' || tokens[0][3] == 'T') &&
            (tokens[0][4] == '\0'))
            return 0;
    }
    if (numTokens > 2 || numTokens < 1) {
        printf("ERROR! Incorrect number of tokens found.\n");
        goto PROMPT;
    }
    for (int i=0; i<numTokens; i++) {
        int count = 0;
        while (tokens[i][count] != '\0') {
            if (count >= MAX_TOKEN) {
                printf("ERROR! Input string too long.\n");
                goto PROMPT;
            }
            count++;
        }
    }
    for (int i=0; i<numTokens; i++) {
        int index = 0;
        int flag = 0;               // set to 1 if char is not a digit
        while (tokens[i][index] != '\0') {
            if (tokens[i][index] < '0' || tokens[i][index] > '9') {
                flag = 1;
            }
            index++;
        }
        if (flag == 0)
            printf("INT ");
        else
            printf("STR "); 
    }
    printf("\n");
    goto PROMPT;
    return 0;
}
