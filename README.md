README -- Assignment 2

This assignment was about tokenizing user input and testing the contents. All programs are compiled using " gcc prog2_X.c" with the exception of program 6 which requires a single integer commandline argument.

Program 1
    This program takes 65 characters of input from STDIN and tokenizes it using ' ' as a delimiter. It then prints each token surrounded with '='s

Program 2
    This prompts the user for 65 characters of input with '>'. It then prints STR or INT for each token. It is an INT if it contains only the digits 0-9 and a STR otherwise.

Program 3
    This program functions the same as program 2, but if no tokens or more than two tokens are entered it prints "ERROR! Incorrect number of tokens found." and prompts the user again.

Program 4
    This program takes one or two inputs in up to 20 character inputs. It prints "ERROR! Incorrect number of tokens found." if there are 0 or more than 2 tokens. It prints "ERROR! Input string too long." if a token is longer than 20 characters. It keeps prompting the user for input until quit is entered (case insensitive).

Program 5
    This program functions the same as program 4, but if one token is entered it prints "STR" or "ERROR! Expected STR." and if two tokens are entered it prints "STR INT" or "ERROR! Expected STR INT."

Program 6
    This program functions the same as program 5, but takes a single integer commandline argument as the number of times the user will be prompted for input. If it is missing or there are more it prints "ERROR! Program 6 accepts 1 command line argument."
